#!/usr/bin/perl -w

#  Do the exists(), keys(), values(), and delete() hash functions
#  work with hashes tied to DBM files?  Let's see!!

use DB_File;  

#  A fifth file permission mask argument is allowed!!
tie %event, "DB_File", "events.db", O_RDWR, 0755 or die "$!\n";

$, = ", ";   #  Dump keys and values with comma as output field separator.
print keys(%event);
print "\n";
print "$key, " while ($key,$val) = each(%event);
print "\n";
print values(%event);
print "\n\n";

print "Hello\n" if exists $event{"Stanley Cup"}; 
print "Good Bye!\n" if defined $event{"Stanley Cup"};

delete $event{"Stanley Cup"};
print keys(%event);

untie %event;
