#!/usr/bin/perl -w
####################  Query the database using hash keys!  ###################
use DB_File;

tie %event, "DB_File", "events.db", O_RDWR or die "Cannot open events.db!\n";

print "Stanley Cup finals are in ", $event{"Stanley Cup"}, "\n";

#  Can I put new stuff in?  Answer should be yes due to O_RDWR permission.
#  Other modes are O_RDONLY and O_WRONLY.
$event{"School Starts"} = "September";

print "World Series is in ", $event{"World Series"}, "\n";  # Fetch!
print "School starts in ", $event{"School Starts"}, "\n";   # Fetch!

untie %event;
